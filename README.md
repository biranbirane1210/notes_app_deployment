# notes_app_deployment
This repository contains a Kubernetes YAML file that can be used to deploy a Notes app on a Kubernetes cluster. The app allows users to create,
read, update, and delete notes.


## Prerequisites

Before using this YAML file, you will need to have the following:
- A Kubernetes cluster
- kubectl installed on your local machine

## Usage
To use this YAML file, follow these steps:

1. Clone the repository to your local machine.
2. Open the `notes-app.yaml` file and modify the values for the environment variables as required.
3. Open a command prompt or terminal window and navigate to the directory where the YAML file is stored.
4. Run the following command to deploy the Notes app on your Kubernetes cluster:

```
kubectl apply -f notes-app.yaml
```
Run the following command to verify that the deployment was successful:
```
kubectl get pods
```
Access the Notes app by opening a web browser and navigating to the IP address or hostname of the Kubernetes node where the app is running.
## Conclusion
With this Kubernetes YAML file, you can easily deploy the Notes app on your Kubernetes cluster. The app provides a simple and convenient way for users to manage their notes in a cloud-native environment.